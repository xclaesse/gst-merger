#! /usr/bin/python3
 
import os
import argparse
import subprocess
from subprocess import call
from subprocess import check_output

global OPTIONS

def move_omx(repo, repodir):
  call(['rm', '-Rf', 'common/'])
  safe_call(['git', 'mv', 'omx', 'tmpomx'])
  safe_call(['git', 'commit', '--amend', '-n', '--no-edit'])
  git_mv(repo, repodir)
  safe_call(['git', 'mv', 'omx/tmpomx', 'omx/omx/'])
  safe_call(['git', 'commit', '--amend', '-n', '--no-edit'])


REPOS = {
  'gst-plugins-base': {
    'dirname': 'base',
    'movematrix':
      [
        ('gst/', 'plugins/gst/'),
        ('ext/', 'plugins/ext/'),
        ('sys/', 'plugins/sys/'),
      ],
  },
  'gst-plugins-good': {
     'dirname': 'good'
   },
   'gst-plugins-bad': {
     'dirname': 'bad'
   },
   'gst-plugins-ugly': {
     'dirname': 'ugly'
   },
   'gstreamer-vaapi': {
     'dirname': 'vaapi'
   },
   'gst-omx': {
     'dirname': 'omx',
     'move-func': move_omx,
   },
   'gst-libav': {
     'dirname': 'ffmpeg',
   },
   'gst-ci': {
     'dirname': 'ci',
   },
   # libs
   'gst-rtsp-server': {
     'dirname': 'rtsp-server',
   },
   'gst-editing-services': {
     'dirname': 'editing-services',
   },
   'gst-devtools': {
     'dirname': 'devtools',
   },
   'gst-docs': {
     'dirname': 'docs',
   },
   'gst-examples': {
     'dirname': 'examples',
   },
   'gst-integration-testsuites': {
     'dirname': 'tests/integration',
   },
   # bindings
   'gst-python': {
     'dirname': 'bindings/python/',
   },
   'gstreamer-sharp': {
     'dirname': 'bindings/csharp',
   },
   'gstreamer-rs': {
     'dirname': 'bindings/rust',
   },
   'gstreamer-rs-sys': {
     'dirname': 'bindings/rust-sys',
   },
   'gst-plugins-rs': {
     'dirname': 'bindings/rust-plugins',
   },
   'gst-build': {
     'dirname': 'devtools/devenv',
   },
}

TOPLEVEL_DIRS = ['core', '.git']
BASE_PATH = os.path.abspath(os.curdir)

def safe_call(args, cwd=None, silent=False):
  if not silent:
    print('%s $ "%s"' % (os.path.basename(os.path.abspath(cwd if cwd else os.curdir)), '" "'.join(args)))
  try:
    return check_output(args, stderr=subprocess.STDOUT, cwd=cwd).decode('utf-8')
  except subprocess.CalledProcessError as e:
    print(e.stdout.decode('utf-8'))
    raise e
  except:
    import ipdb; ipdb.set_trace()

def is_toplevel(repodir, f):
  return f in TOPLEVEL_DIRS

def get_merged_files(p=False):
  # Extract list of files added from previous merged commit
  stats = safe_call(['git', 'diff', '--name-only', 'HEAD^..HEAD'])
  if p:
    print(stats)
  res= [f for f in stats.split('\n') if os.path.exists(f)]
  return res

def git_mv(repo, repodir, all=False):
  call(['rm', '-Rf', 'common/'])
  os.makedirs(repodir, exist_ok=True)
  if not all:
    to_move = get_merged_files()
  else:
    to_move = [f for f in os.listdir(os.curdir) if not is_toplevel(repodir, f)]
  if [t for t in to_move if '..' in t]:
    to_move = get_merged_files(True)
    import ipdb; ipdb.set_trace()
  for f in to_move:
    dirname = os.path.dirname(f)
    if dirname:
      _dir = os.path.join(repodir, dirname)
      os.makedirs(_dir, exist_ok=True)
    else:
      _dir = repodir

    try:
      safe_call(['git', 'mv', f, _dir + '/'])
    except:
      import ipdb; ipdb.set_trace()
  safe_call(['git', 'commit', '--no-edit', '-n', '-m', 'Move files from %s into  the "%s/" subdir' % (repo, repodir)])
  print(safe_call(['ls']))

def remote_add(repo):
  remotes = safe_call(['git', 'remote', '-v'], silent=True)
  if False: # os.path.exists(BASE_PATH + '/../' + repo) and 'integration-testsuites' not in repo:
    remote = BASE_PATH + '/../' + repo
    if remote in remotes:
      print('Remote %s already exists' % remote)
      return False
    # safe_call(['git', 'fetch', 'origin'], cwd=remote)
    safe_call(['git', 'remote', 'add', repo, remote])
  else:
    remote = 'https://gitlab.freedesktop.org/gstreamer/' + repo
    if remote in remotes:
      print('Remote %s already exists' % remote)
      return False
    safe_call(['git', 'remote', 'add', repo, remote])

  return True

def clone_core_if_needed():
  repo = 'gstreamer'
  repo_name = 'gstreamer_merged'
  if not os.path.exists(repo_name):
    if os.path.exists('../' + repo):
      safe_call(['cp', '-r', '../' + repo, repo_name])
      safe_call(['git', 'fetch', 'origin'], cwd=repo_name)
      call(['git', 'checkout', 'one_repo'], cwd=repo_name)
      safe_call(['git', 'reset', '--hard', 'origin/master'], cwd=repo_name)
      safe_call(['git', 'clean', '-fdx'], cwd=repo_name)
      safe_call(['git', 'config', 'lfs.url', 'https://gitlab.freedesktop.org/gstreamer/gst-integration-testsuites.git/info/lfs'], cwd=repo_name)
    else:
      safe_call(['git', 'clone', 'https://gitlab.freedesktop.org/gstreamer/' + repo, repo_name])
  else:
    print('GStreamer core already in %s')

  os.chdir(repo_name)

def repo_is_merged(repo, repodir):
  return remote_add(repo) is False and os.path.exists(repodir)

def merge_repos_if_needed():
  if not repo_is_merged('gstreamer', 'core'):
    git_mv('gstreamer', 'core', all=True)
  else:
    print(' -> INFO: gstreamer core ALREADY MERGED')

  for repo, data in REPOS.items():
    repodir = data['dirname']
    if remote_add(repo) is False and os.path.exists(repodir):
      print(' -> INFO: %s ALREADY MERGED into %s' % (repo, repodir))
      continue
    safe_call(['git', 'remote', 'update', repo])
    try:
      safe_call(['git', 'merge', '--no-edit', '--allow-unrelated-histories', repo + '/master', '-m', 'Merging %s' % repo])
    except subprocess.CalledProcessError as e:
      # Workaround stupid merge issue in -bad
      if "COPYING deleted in HEAD" in e.stdout.decode('utf-8'):
        safe_call(['git', 'add', 'COPYING'])
        call(['git', 'commit', '-a', "-n"])
      else:
        raise e
    call(['git', 'commit', '-a', "-n", "--amend"])
    data.get('move-func', git_mv)(repo, repodir)
    print(safe_call(['git', 'status']))


def move_files_from_core():
  call(['git', 'mv', 'core/.gitignore', '.'])
  call(['git', 'mv', 'core/hooks', '.'])
  call(['git', 'add', '.gitignore'])
  call(['git', 'add', 'hooks/'])

def setup_meson():
  safe_call(['cp', '../meson.build', '.'])
  safe_call(['git', 'add', 'meson.build'])

def merge_options():
  call(['git', 'rm', 'meson_options.txt', '-f'])
  call(['git', 'checkout', '--', 'core/meson_options.txt'])
  call(['git', 'mv', 'core/meson_options.txt', '.'])
  for repo, data in REPOS.items():
    repodir = data['dirname']
    optionfile = os.path.join(repodir, 'meson_options.txt')
    if os.path.exists(optionfile):
      with open('meson_options.txt', 'a') as f:
        with open(optionfile, 'r') as repo_options:
          f.write(repo_options.read())
      safe_call(['git', 'rm', optionfile])
  safe_call(['git', 'add', 'meson_options.txt'])


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument('-u')

  global OPTIONS
  OPTIONS = parser.parse_args()
  clone_core_if_needed()
  peditor = os.environ.get('GIT_EDITOR')
  os.environ['GIT_EDITOR'] = 'true'
  merge_repos_if_needed()
  if peditor:
    os.environ['GIT_EDITOR'] = peditor
  else:
    del os.environ['GIT_EDITOR']
  # move_files_from_core()
  # setup_meson()
  # merge_options()